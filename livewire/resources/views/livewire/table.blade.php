<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>

	<h1>Lista de Post</h1>

	<table class="table">
		  <thead>
		  	 <tr>
		  	 	<td>ID</td>
		  	 	<td>Titulo</td>
		  	 	<td>Contenido</td>
		  	 	<td colspan="2">&nbsp;</td>
		  	 </tr>
		  </thead>
		  <tbody>
		  	  @foreach($posts as $post)

		  	   <tr>
		  	   	  <td>{{$post->id}}</td>
		  	   	  <td>{{$post->title}}</td>
		  	   	  <td>{{$post->body}}</td>

		  	   	  <td>
		  	   	  	<button wire:click="edit({{$post->id}})" class="btn btn-primary">Editar</button>
		  	   	  </td>

		  	   	  <td>
		  	   	  	<button wire:click="destroy({{$post->id}})" class="btn btn-danger">Eliminar</button>
		  	   	  </td>
		  	   </tr>
               
		  	  @endforeach
		  </tbody>
	</table>

	<!-- {{$posts->links()}} -->
	
	
</body>
</html>